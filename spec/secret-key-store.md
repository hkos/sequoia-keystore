---
title: Sequoia's Secret Key Store
docname: draft-nhw-sequoias-secret-key-store-00
date: 2022-04-15
category: info

ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft

stand_alone: yes
pi: [toc, sortrefs, symrefs]

author:
 -
    ins: N.H. Walfield
    name: Neal H. Walfield
    org: Sequoia PGP
    email: neal@sequoia-pgp.org
normative:
 RFC2119:
 RFC4880:
 RFC8174:
--- abstract

The secret key store is an optional daemon provided by Sequoia, which
manages secret keys.

--- middle

# Introduction

The secret key store is a long-running daemon.  It provides two
abstract interfaces, an application-facing interface and a device
driver-facing interface.  The daemon glues these two interfaces
together, and multiplexes access to the keys.

The application-facing interface provides applications a way to
enumerate, manage, and use secret keys independent of where the keys
are stored and how they are actually accessed.  The device
driver-facing interface is an interface that different
backends---device drivers---implement.

Some backends are:

  - Soft keys (i.e., in-memory keys),
  - Keys on hardware security modules (HSMs) like smartcards and TPMs,
  - Remote keys like soft keys owned by a different user account and
    accessed via a daemon, and keys on a different host, which are
    accessed via ssh.

Using a daemon instead of a library or a sub-process, which is spawned
once per application and is terminated when the application
terminates, offers several advantages.

The main user-visible advantage is that the daemon is able to hold
state.  In the case of soft keys, the daemon can cache an unencrypted
key in memory so that the user doesn't have to unlock the key as
frequently.  This is particularly helpful when a command-line tool
like sq is executed multiple times in a row and each time accesses the
same password-protected key.  Likewise, a daemon can cache a PIN
needed to access an HSM.  It can also keep the HSM open thereby
avoiding the initialization overhead.  This also applies to remote
keys: an ssh tunnel, for instance, can be held open, and reused as
required.

A separate daemon also simplifies an important non-functional security
property: process separation.  Since soft keys aren't managed by the
application, but by the daemon, an attacker is not able to use a
heartbleed-style attack to exfiltrate secret key material.

## Objects

The secret key store uses three user-visible abstractions: backends,
which are basically device drivers, devices, and keys.  When
instantiated, the objects conceptually form a hierarchy.  An example
hierarchy is:

```
                            Secret Key Store
                         /          |          \
                     /              |                \
        soft key              openpgp card                  ssh         Backends
           |                  /           \                  |
        keys.db            Gnuk         Nitro Key        user@host      Devices
                         #123456         #234567
        /      \        /   |   \       /   |   \       /      \
     0x10     0x23   0x31 0x49 0x5A  0x64 0x71 0x88   0x98    0xAB      Keys
```

In short, the secret key store contains a number of drivers.  Each
driver has zero or more devices, which may or may not be connected at
any given time, and each device has zero or more keys.

Individual devices may be locked.  For instance, a soft key database
like `keys.db` in the above figure may be encrypted and require a
password before it can enumerate any keys that it contains.  Likewise,
individual keys may be locked.  They may be protected by a password,
or a presense sensor.

## Scope

Some backends have a lot of device-specific functionality.  For
instance, a key stored on a TPM can be exported in encrypted form, and
imported by a TPM on another computer.  It is not the intention of
this API to support this type of advanced functionality.  Instead, a
special tool should be used to do these types of operations.

# Core Functionality

In addition to decrypting and signing text, we need the following
functionality:

  - List known keys.
  - Add a key.
  - Modify a known key.
  - Remove a key.
  - Unlock a key so that it can be used.

# Types

  - backend_t - A string identifying a backend, e.g. "key", "pkcs11",
    or "ssh".

  - backend_param_t - A string that can be created and interpreted by
    an (advanced) user, and is parsed by the backend, e.g.,
    "password='12345687' ttl=15m", or
    "user@host/C7966E3E7CE67DBBECE5FC154E2AD944CFC78C86".

  - key_t - A binary OpenPGP Public Key or Public Subkey packet.

  - device_handle_t - An opaque string used to address a device.  This
    string is determined by the backend, and, in the context of a
    backend, it must be unique.  The string should be fixed for the
    life of the device.  That is, a device should be addressable using
    the same device_handle_t at least from the point that a key is
    registered until no more keys are registered even if the secret
    key store is restarted.

  - key_handle_t - Like a device_handle_t, but used to address a key
    not a device.

  - device_description_t - A free-form string that describes a device.
    The default value is set by the backend when the key is registered
    (e.g., "Gnuk Serial Number 18347890"), but may be overridden by
    the user (e.g., "Green Gnuk").  The value is persisted by the
    secret key store.

  - key_description_t - Like a device_description_t, but used to
    describe a key not a device.

  - message_t - A string intended to be shown to the end user and not
    parsed by the application, e.g., "Invalid PIN, you have two more
    tries before the device is locked."

  - text_t - A binary string.  This is used for passing plaintext or
    ciphertext.

# Device Driver API

  - Initialize the backend.

      backend_init() -> Result<backend_t>

  - Scan for devices.

      backend_scan() -> Result<array of device_handle_t>

    Causes the backend to search for devices.  For some backends, only
    registered devices will ever be returned.  For other backends, like
    one managing smartcards, this may also return devices that are
    available and not yet registered.

    This function should not initialize the device for use, e.g., it
    should not create an ssh tunnel.

  - Forget a device.

      backend_unregister_device(device_handle_t) -> Result<>

    Unregisters a device managed by the backend.  This should not
    destroy any secret key material stored on the device.

    Note: devices are registered using a backend-specific tool.

  - Unlock a device.

      device_unlock(device_handle_t, backend_param_t) -> Result<>

    Connects to and unlocks a device.

    Some devices need to be initialized.  For instance, to access a
    remote key, it may be necessary to create an ssh tunnel.  Some
    devices need to be unlocked before the keys can be enumerated.
    For instance, if soft keys are stored in a database and the
    database is encrypted, it may be necessary to supply a password to
    decrypt the database.  In this case, the parameter might be
    "password='1234'".

  - Lock a device.

      device_lock(device_handle_t) -> Result<>.

    Locks the device if it has been previously unlocked.  If the
    device is locked or can't be locked, this is a noop.  If the
    device needs to be deinitialized, it MAY be deinitialized lazily
    if doing so cannot result in a user-visible error.  For instance,
    if the device uses an ssh tunnel, the ssh tunnel may be closed
    later.  A smartcard, however, should be released immediately to
    allow other processes to use it.

  - List keys on a device.

      device_list_keys(device_handle_t) -> Result<array of key_handle_t>

    Lists the keys on the device.

    Some of the returned keys may be known, but not currently
    available.  For instance, if a smartcard is not plugged in, or an
    ssh connection is not established.

  - Delete a key.

      key_delete(key_handle_t) -> Result<>

    Deletes the key.  This destroys the key's secret key
    material.

    Note: keys are generated and imported using a backend-specific
    tool.

  - Unlocks a key.

      key_unlock(key_handle_t, backend_params_t) -> Result

    A key is typically unlocked by providing a password or pin.  Not
    all keys are locked.  If the key is not available, this should
    attempt to connect to the device.  If the device is not available
    or cannot be initialized, then this should fail.

  - Lock a key (optional)

      key_lock(key_handle_t) -> Result

    Relocks the key.  This usually causes the backend to forget the
    key's password.  If the key can't be locked or is already locked,
    this is a noop.

  - Decrypt a message.

      decrypt_message(key_handle_t, text_t) -> Result<text_t>

      Text is the message to decrypt.  This is normally an encrypted
      session key as taken from a PKESK or SKESK.

  - Sign a message.

      sign_message(key_handle_t, text_t) -> Result<text_t>
      WK: Same as above, maybe the parameter to be signed should be a digest instead of text?

    text is the message to sign.

  - Upcall: an operation has completed.

      done(operation_t, Result<description_t>)

    A pending operation has completed.

# Application API

  - List the available backends.

      backends_list() -> Result<array of backend_t>

  - Scan for devices.

      backend_scan(backend_t) -> Result<array of device_handle_t>

    Causes the backend to search for devices.  For some backends, only
    registered devices will ever be returned.  For other backends, like
    one managing smartcards, this may also return devices that are
    available and not yet registered.

    This function should not initialize the device for use, e.g., it
    should not create an ssh tunnel.

  - Register a device.

      backend_register_device(backend_t, params_t) -> Result<(device_handle_t, device_description_t)>

    Registers a device.  This is particularly useful when a device
    like an ssh tunnel or a file can't or shouldn't be discovered by
    backend_scan.

  - Forget a device.

      backend_unregister_device(backend_t, device_handle_t) -> Result<>

    Unregisters a device from the backend.  This should not destroy any secret
    key material stored on the device.

  - Unlock a device.

      device_unlock(device_handle_t, backend_param_t) -> Result<>

    Connects to and unlocks a device.

    Some devices need to be initialized.  For instance, to access a
    remote key, it may be necessary to create an ssh tunnel.  Some
    devices need to be unlocked before the keys can be enumerated.
    For instance, if soft keys are stored in a database and the
    database is encrypted, it may be necessary to supply a password to
    decrypt the database.  In this case, the parameter might be
    "password='1234'".

  - Lock a device.

      device_lock(device_handle_t) -> Result<>.

    Locks the device if it has been previously unlock.  If the device
    is locked or can't be locked, this is a noop.  If the device needs
    to be deinitialized, it MAY be deinitialized lazily if doing so
    cannot result in a user-visible error.  For instance, if the
    device uses an ssh tunnel, the ssh tunnel be closed later.  A
    smartcard, however, should be released immediately.

  - List keys on a device.

      device_list_keys(device_handle_t) -> Result<array of key_handle_t>

    Lists the keys on the device.

    Some of the returned keys may be known, but not
    currently available.  For instance, if a smartcard is not plugged
    in, or an ssh connection is not established.

  - Delete a key.

      key_delete(key_handle_t) -> Result<>

    Deletes the key.  This destroys the key's secret key
    material.

  - Unlocks a key.

      key_unlock(key_handle_t, backend_params_t) -> Result

    A key is typically unlocked by providing a password or pin.  Not
    all keys are locked.  If the key is not available, this should
    attempt to connect to the device.  If the device is not available
    or cannot be initialized, then this should fail.

  - Lock a key (optional)

      key_lock(key_handle_t) -> Result

    Relocks the key.  This usually causes the backend to forget the
    key's password.  If the key can't be locked or is already locked,
    this is a noop.

  - Decrypt a message.

      decrypt_message(key_handle_t, array of pkesk_t) -> Result<text_t>

    The secret key store will try to decrypt the pkesks in an
    arbitrary order.  When it succeeds in decrypting a pkesk, it stops
    and returns the session key.  By not enforcing an order, the
    secret key store is able to first try keys that are immediately
    available, and only try keys that need to be unlocked or connected
    if that fails.

  - Sign a message.

      sign_message(key_handle_t, text_t) -> Result

    plaintext is the message to sign.
